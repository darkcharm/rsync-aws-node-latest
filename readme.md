# Node.js with rsync and aws

## Base Docker image
* [library/node](https://hub.docker.com/r/library/node/)

Latest node.js.
Pre-installed rsync.
Pre-installed aws.

Used for AWS deployments.
